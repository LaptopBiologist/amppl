# AUTOGENERATED! DO NOT EDIT! File to edit: 33_experimental_mixtures.ipynb (unless otherwise specified).

__all__ = ['MixtureDistribution']

# Cell
class MixtureDistribution():
    def __init__(self,wt=None, compdists=[], log_wt=None):
        """A class for constructing mixtures of multiple component distributions.

        ****
        **Arguments:**

        `wt` : The mixing weights

        `log_wt`: Alternative to wt, the weights may be provided in log scale

        `comp_dist` (list): A list of component distributions. Each must have \
        .logpdf and .rvs methods

        """
        if isinstance(log_wt, type(None))==False:
            self.wt=numpy.exp(log_wt)
            self.log_wt=log_wt
            self.has_log_wts=True
        else:
            self.wt=wt
            self.has_log_wts=False
        self.compdists=compdists

    def component_logpdf(self, x):
        """Compute the log-probability of observations for each component distribution

        ****
        **Arguments:**

        `x` (N-by-p array): The N observations, each of which has p dimensions.

        ****
        **Returns**

        logprobs (N-by-D array): The log-probabilities of the N observations for \
        each of the D component distributions.

        """
        logprobs=[]
        for i in range(len(self.wt)):
            if self.has_log_wts==False:
                comp_prob=numpy.log(self.wt[i])+self.compdists[i].logpdf(x )
            else:
                comp_prob=self.log_wt[i]+self.compdists[i].logpdf(x )
            logprobs.append(comp_prob)
        logprobs=numpy.vstack(logprobs)
        return logprobs
    def logpdf(self,x):
        """Compute the log-probability of observations under the mixture distribution

        ****
        **Arguments:**

        `x` (N-by-p array): The N observations, each of which has p dimensions.

        ****
        **Returns**

        logprobs (N array): The log-probabilities of the N observations.

        """
        logprobs=self.component_logpdf(x)
        return scipy.special.logsumexp(logprobs, axis=0)


    def rvs(self, size=1):
        """Sample from the mixture

        ****
        **Arguments:**

        `size` (int): the number of observations to draw.

        ****
        **Returns:**

        samples (N-by-p array): the sampled values"""
        samples=[]
        for i in range(len(self.wt)):
            samp=self.compdists[i].rvs( size=size )

            samples.append(samp)
        if len(samples[0].shape)>1:
            samples=numpy.dstack(samples)
        else:
            samples=numpy.vstack(samples).T

        wts=numpy.nan_to_num(self.wt, 1e-7)
        wts=self._stable_multinomial_pvals(wts/wts.sum())
        ind=scipy.stats.multinomial.rvs(p=wts, n=1, size=size)
        obs_ind,comp_ind= numpy.where(ind==1)
#         print (samples.shape)
        if len (samples.shape)==3:
            return samples[obs_ind,:,comp_ind]
        else:
            return samples[obs_ind,comp_ind]

    def _stable_multinomial_pvals(self,p, err=1e-6):
        """Prevent rounding errors in the multinomial probabilities."""

        overflow=1./p[:-1].sum()
        p=p/overflow
        p[-1]+=err
        return p/p.sum()