# amPPL
> A modest probabilistic programming language (PPL) I threw together because I wanted to be able to use JAX to evaluate likelihoods, but was making way too many mistakes when writing likelihoods.


## Install

## How to use

```python
import numpy
import scipy
import scipy.stats

from matplotlib import pyplot
import seaborn
from tqdm import tqdm
import jax.scipy as jsc
import jax.numpy as jnp
from jax import grad, jit, value_and_grad

from jax.experimental import optimizers
```

    /home/mpm289/anaconda3/envs/txmap/lib/python3.8/site-packages/jax/experimental/optimizers.py:28: FutureWarning: jax.experimental.optimizers is deprecated, import jax.example_libraries.optimizers instead
      warnings.warn('jax.experimental.optimizers is deprecated, '


```python
from amppl.model import Model, ModelBase

```

Let's generate some data that follows a simple linear model of the form

{% raw %}
$$y= -\frac{1}{2}X + 5$$
{% endraw %}

```python
X=scipy.stats.norm.rvs(0, 5, size=50)
beta0, beta1=5, -.5

def lin_fxn(X, beta0, beta1):
    return X*beta1+beta0
y_true=lin_fxn(X, beta0, beta1)
sd=1.5
err= scipy.stats.norm.rvs(0,sd, size=len(X))

y_obs=y_true+err

pyplot.scatter(X,y_obs, )
domain=numpy.linspace(X.min(),X.max(), 100)
pyplot.plot(domain, lin_fxn(domain, beta0, beta1), c='black', lw=2 )
```




    [<matplotlib.lines.Line2D at 0x7f45d8852be0>]




![png](docs/images/output_5_1.png)


```python
err_val=numpy.linspace(0,10,100)

pyplot.plot(err_val,numpy.exp(jsc.stats.gamma.logpdf(err_val, a=1.5,loc=0, scale=4)))
```

    WARNING:absl:No GPU/TPU found, falling back to CPU. (Set TF_CPP_MIN_LOG_LEVEL=0 and rerun for more info.)





    [<matplotlib.lines.Line2D at 0x7f45c3ef7bb0>]




![png](docs/images/output_6_2.png)


I want to be able to define the model in terms of probability distributions without have to keep track of any of the math. Normally I would use PyMC3 for this sort of task. However, sometimes when I have a very large dataset or a complicated model I want the flexibility to be able to do my own optimization, using JAX to compute the gradients.

We can specify models in a similar manner to pyMC. We need to create a class using `ModelBase` as a template and using the `@Model` decorator. The decorator runs some initialization code. We define the model's structure by including a  method named `model`. This, counterintuitively, should not return values. Rather, all of the computed probabilities will be stored internally.

```python
@Model
class LinearModel(ModelBase):
    def model(self, *args, **kwargs):
        #Define the priors
        beta=self.Normal('beta', shape=2, loc=0,scale=10)
        err=self.Gamma('err', a=1.5, scale=4)
        
        #Compute the predicted values of Y
        y_pred=lin_fxn(X, beta[0], beta[1])
        
        #Define the likelihood by creating an RV with a predefined value equal to y_obs
        obs=self.Normal('Y_obs', 
                    value=y_obs, loc=y_pred, scale=err)
```

By building the class around the `ModelBase` template, we have access to a number of methods that we don't need to explicitly define. The principal method we need to define the model is the `ModelBase.RV` function. This adds parameters to the model when `model` is first called, stores the probability of the current value of each parameter, and returns current value of the parameter. This is wrapped for a number of predefined distributions in the `DistributionDefinitions` base class, on which `ModelBase` is built. So, when write

```python
        beta=self.Normal('beta', shape=2, loc=0,scale=10)
```

under the surface the model

1. Looks up the current value of `beta` stored in its internal state. If the variable doesn't exist, it creates it.

2. Computes the log-probability of the current value under a Normal distribution with mean of 0 and standard deviation of 10. It stores this internally.

3. Return the current value of `beta`. This way, we can perform mathematical operations with `beta` and pass the results to other probability distributions.

The model always works with unconstrained representations of its parameters and stores them as an array. That is `err` follows a Gamma distribution and must always be positive. So, the model works with the log of `err`. If we want to create an array of the appropriate size for this internal state, we can use `generate_vector`.

```python
x0=LinearModel.generate_vector()
```

And we can pass this to the model with `ModelBase.set_state_and_evaluate` to get the associated probability.

```python
LinearModel.set_state_and_evaluate(x0)
```




    DeviceArray(-798.9352, dtype=float32)



This also sets the model's internal state. We can access the corresponding (constrained) model parameters in the `.parameters` attribute

```python
LinearModel.parameters
```




    {'beta': DeviceArray([0., 0.], dtype=float32),
     'err': DeviceArray([1.], dtype=float32),
     'Y_obs': DeviceArray([0.], dtype=float32)}



If we want to perform gradient descent, we can import the `Optimizer` class from the `optimizers` submodule:

```python
from amppl.optimizers import Optimizer
```

```python
opt=Optimizer(LinearModel)
```

```python
opt.fit(500)
```

     37%|███▋      | 185/500 [00:01<00:02, 152.14it/s]


    converged


The above model has two kinds of RV:
    
1. RVs that do not have their values defined (`beta` and `err`). These are free parameters whose values we will try to estimate.

1. RVs that have a defined value using the `value` argument (`obs`). These are not 

```python
pyplot.plot(numpy.log10(opt.hist))
```




    [<matplotlib.lines.Line2D at 0x7f45c33eb040>]




![png](docs/images/output_21_1.png)


We can check fitted model parameters using `.vector2values`

```python
fitted_params=opt.parameters
```

```python
pyplot.scatter(X,y_obs, )
domain=numpy.linspace(X.min(),X.max(), 100)
pyplot.plot(domain, lin_fxn(domain, beta0, beta1), c='black', lw=2, ls='--', label='True relationship' )
pyplot.plot(domain, lin_fxn(domain, fitted_params['beta'][0], fitted_params['beta'][1]), 
            c='red', lw=2, ls='-', label='Fitted relationship' )
pyplot.legend(fontsize=14)
```




    <matplotlib.legend.Legend at 0x7f45c34cdfd0>




![png](docs/images/output_24_1.png)


Let's see how this compares to the results from fitting the regression with scipy

```python
linreg=scipy.stats.linregress(x=X, y=y_obs)

pyplot.plot(domain, lin_fxn(domain, fitted_params['beta'][0], fitted_params['beta'][1]), c='red', lw=4, ls='-' )
pyplot.plot(domain, lin_fxn(domain, linreg.intercept, linreg.slope), c='black', lw=4, ls='--' )
pyplot.scatter(X,y_obs, )

```




    <matplotlib.collections.PathCollection at 0x7f45c32fe8b0>




![png](docs/images/output_26_1.png)


### A more complicated example

Instead, we could generate a million data points from a hierarchical model where each data point comes from one of 100 different groups, each with its own slope and intercept. These slopes and intercepts, however, each arise from normal distributions with unknown means and variances.

```python



ndata=1000000
ngroups=100
X=scipy.stats.norm.rvs(0,2.5, size=ndata)
group=numpy.repeat(numpy.arange(ngroups), ndata/ngroups).astype(int)
beta1=scipy.stats.norm.rvs(2,.7, size=ngroups)
beta0=scipy.stats.norm.rvs(-3,1, size=ngroups)
err=scipy.stats.norm.rvs(0,2, size=ndata)
Y=X*beta1[group]+beta0[group]+err
```

```python
@Model
class HierarchicalModel(ModelBase):

    def model(self, *args, **kwargs):
        
        #define the hyperpriors for the mean coefficient across all groups
        mean_0=self.Normal('mean_0',loc=0, scale=5 )
        mean_1=self.Normal('mean_1',loc=0, scale=5 )
        
        #define the hyperpriors for the standard deviation of the coefficients across groups
        sd_0=self.Gamma('sd_0',a=1, scale=3 )
        sd_1=self.Gamma('sd_1',a=1, scale=3)
        
        #define the priors on the regression coefficients for each group
        beta_0=self.Normal('beta_0',shape=ngroups,loc=mean_0, scale=sd_0 )
        beta_1=self.Normal('beta_1',shape=ngroups,loc=mean_1, scale=sd_1 )
        
        #define the prior on the residuals
        err=self.Gamma('err',a=1, loc=0, scale=3)
        
        # compute the model's predictions of Y given the coefficients
        y_pred=beta_0[group]+beta_1[group]*X

        # Evaluate the likelihood of the observed data
        obs=self.Normal('Y', value=Y,loc=y_pred, scale=err)
```

```python
hier_opt=Optimizer(HierarchicalModel)
```

```python
hier_opt.fit(10000)
```

      2%|▏         | 193/10000 [00:06<05:24, 30.18it/s]


    converged


```python
fitted_params=hier_opt.parameters
fitted_params
```




    {'mean_0': DeviceArray([-3.0702853], dtype=float32),
     'mean_1': DeviceArray([1.8884606], dtype=float32),
     'sd_0': DeviceArray([1.020398], dtype=float32),
     'sd_1': DeviceArray([0.67489374], dtype=float32),
     'beta_0': DeviceArray([-3.5184858 , -2.5256538 , -3.2049334 , -1.7453911 ,
                  -2.9334803 , -3.2075202 , -3.1363032 , -3.628992  ,
                  -1.3097519 , -3.5495193 , -2.9911895 , -2.8854225 ,
                  -2.562002  , -3.607844  , -2.2766595 , -3.7782514 ,
                  -3.362463  , -2.0848649 , -2.3548937 , -4.036095  ,
                  -4.4088273 , -2.9668596 , -2.9125679 , -2.3137417 ,
                  -1.3718183 , -2.047501  , -1.1800492 , -4.4469447 ,
                  -4.684188  , -3.731158  , -3.3179586 , -2.9957335 ,
                  -2.5399263 , -2.8553712 , -3.9450946 , -4.9130716 ,
                  -2.362961  , -3.929983  , -4.4871006 , -4.4110217 ,
                  -3.3446145 , -2.570153  , -1.7968554 , -1.0908191 ,
                  -3.4272904 , -3.544214  , -1.878356  , -4.4506617 ,
                  -4.5542054 , -2.5242746 , -3.8132043 , -3.14976   ,
                  -0.4503347 , -3.8067794 , -3.1678686 , -3.9180384 ,
                  -3.3595243 , -1.7972159 , -3.9707065 , -2.7395163 ,
                  -3.96301   , -3.8225946 , -4.774903  , -2.4798768 ,
                  -4.102974  , -0.79226583, -3.1558688 , -3.2069879 ,
                  -3.456676  , -3.7014122 , -2.412377  , -2.8310876 ,
                  -4.083839  , -1.4566938 , -3.2204645 , -4.1494484 ,
                  -2.907036  , -1.581706  , -5.024054  , -4.601529  ,
                  -2.006185  , -1.4074519 , -2.2719555 , -3.2233696 ,
                  -2.0476284 , -2.711845  , -0.74490196, -2.8920257 ,
                  -3.8096027 , -3.2701383 , -2.8061042 , -4.813335  ,
                  -3.6228034 , -3.4719844 , -4.9968367 , -2.8489559 ,
                  -2.1463306 , -2.2256515 , -2.7039073 , -3.716985  ],            dtype=float32),
     'beta_1': DeviceArray([2.3846827 , 1.4345671 , 2.502346  , 1.4018782 , 2.0923035 ,
                  0.22129573, 1.8758335 , 2.3550327 , 1.9830874 , 1.4261016 ,
                  1.9161296 , 2.5862045 , 2.2304611 , 2.7068007 , 1.5956476 ,
                  0.4503849 , 3.1404843 , 2.4414928 , 1.5892748 , 2.276022  ,
                  1.6207057 , 2.083146  , 1.5255673 , 2.0419855 , 1.9183838 ,
                  2.4078395 , 1.5190381 , 3.1096401 , 2.861902  , 2.2595503 ,
                  1.205109  , 1.7326455 , 2.8401747 , 2.2501922 , 2.8984258 ,
                  2.6888466 , 1.8168633 , 1.0749964 , 2.2355163 , 2.3586268 ,
                  1.004639  , 0.948896  , 1.0426489 , 1.8247811 , 1.5786376 ,
                  2.05364   , 1.2768892 , 1.4964155 , 0.81480557, 0.57397825,
                  0.39817804, 1.4727576 , 1.7694899 , 2.330979  , 1.8216158 ,
                  2.2751305 , 2.904385  , 2.0630717 , 2.2183554 , 2.9583833 ,
                  1.4656827 , 2.2222526 , 0.52969027, 2.500483  , 1.1661296 ,
                  1.9620643 , 2.3497567 , 2.1054811 , 1.6217408 , 1.7839227 ,
                  0.87832755, 2.5813053 , 2.1067307 , 2.1177485 , 1.62527   ,
                  1.4225414 , 2.0208063 , 3.4122515 , 2.4260154 , 1.2639897 ,
                  1.9539875 , 2.0384572 , 0.68817043, 2.4840043 , 2.4698806 ,
                  2.8549645 , 2.7949815 , 1.1063803 , 1.3629808 , 0.84574354,
                  2.5149162 , 1.8086867 , 1.5183532 , 1.1462862 , 1.0842497 ,
                  1.4311299 , 2.6063454 , 2.609889  , 2.1066437 , 2.2766566 ],            dtype=float32),
     'err': DeviceArray([1.9969463], dtype=float32),
     'Y': DeviceArray([], dtype=float32)}



Let's check whether we've recovered the hyperparameters:

```python
pyplot.scatter([fitted_params['mean_0'],fitted_params['mean_1']],[-3, 2],s=80, c='r', label='group_means' )
pyplot.scatter([fitted_params['sd_0'],fitted_params['sd_1']],[1, .7],s=80, c='b', label='group_SDs' )
pyplot.plot([-3,2],[-3,2], c='black', lw=2, zorder=-1, ls='--')
pyplot.gca().set_aspect('equal')
pyplot.ylabel('True Parameter', size=14)
pyplot.xlabel('Estimated Parameter', size=14)
pyplot.legend()
```




    <matplotlib.legend.Legend at 0x7f45c31d87c0>




![png](docs/images/output_34_1.png)


Yup, we have.

```python
pyplot.figure(figsize=(10,5))
pyplot.subplot(121)
pyplot.scatter(fitted_params['beta_0'],beta0,s=10, c='r')
# pyplot.plot([-3,2],[-3,2], c='black', lw=2, zorder=-1, ls='--')
pyplot.gca().set_aspect('equal')
pyplot.ylabel('True Parameter', size=14)
pyplot.xlabel('Estimated Parameter', size=14)
pyplot.title('Intercepts')
pyplot.subplot(122)
pyplot.scatter(fitted_params['beta_1'],beta1,s=10, c='r')
# pyplot.plot([-3,2],[-3,2], c='black', lw=2, zorder=-1, ls='--')
pyplot.gca().set_aspect('equal')
pyplot.title('Slopes')
pyplot.xlabel('Estimated Parameter', size=14)
```




    Text(0.5, 0, 'Estimated Parameter')




![png](docs/images/output_36_1.png)


Note, that this kind of hierarchical model can be nonrobust with respect to optimization if the number of observations per group is small. The posterior volume associated with a given hyperparameter value needs to be accounted for.
